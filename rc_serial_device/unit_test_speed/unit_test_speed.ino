#include <DueTimer.h>
#include "action.h"
int PwmPin_1=7;//In1_1
int PwmPin_2=8;//In1_2
int PwmPin_3=9;//In2_1
int PwmPin_4=10;//In2_2

int interrupt_1=2;
int interrupt_2=3;

//初始化LED状态
bool state_1=false;
bool state_2=false;

int LED1=13;
int LED2=14;

void blink_1(){
  state_1=!state_1;//反转LED_1电位
}
void blink_2(){
  state_2=!state_2;//反转LED_2电位
}
void setup() {
  //初始化输出口
  pinMode(PwmPin1, OUTPUT);
  pinMode(PwmPin2, OUTPUT);
  pinMode(PwmPin3, OUTPUT);
  pinMode(PwmPin4, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  
 //初始化外部中断口
  pinMode(interrupt_1, INPUT);
  pinMode(interrupt_2, INPUT);

  //启用外部中断
  attachInterrupt(interrupt_1,blink,interrupt_1);
  attachInterrupt(interrupt_1,blink,interrupt_2);

  //输出0101状态，两电机全速加速
  digitalWrite(PwmPin1,HIGH);
  digitalWrite(PwmPin2,LOW);
  digitalWrite(PwmPin3,HIGH);
  digitalWrite(PwmPin4,LOW);
}

void loop() {
 //轮询state_1和state_2
  if(state_1)
    digitalWrite(LED1,HIGH);
  else
    digitalWrite(LED1,LOW);
  if(state_2)
    digitalWrite(LED2,HIGH);
  else
    digitalWrite(LED2,LOW);
}
