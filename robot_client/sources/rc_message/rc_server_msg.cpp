//
// Created by PulsarV on 18-10-30.
//
#include <rc_message/rc_msg_server.h>

namespace rccore {
    namespace message {
        MessageServer::MessageServer(size_t image_message_queue_size,
                                     size_t serial_message_queue_size,
                                     size_t gyro_message_queue_size,
                                     size_t radar_message_queue_size,
                                     size_t network_message_queue_size,
                                     size_t move_message_queue_size) :
                pimageMessage(new ImageMessage(image_message_queue_size)),
                pserialMessage(new SerialMessage(serial_message_queue_size)),
                pnetworkMessage(new NetworkMessage(network_message_queue_size)),
                pmoveMessage(new MoveMessage(move_message_queue_size)),
                pradarMessage(new RadarMessage(radar_message_queue_size)),
                pgyroMessage(new GyroMessage(move_message_queue_size)) {

        }

        MessageServer::~MessageServer() {

        }
    }
}
