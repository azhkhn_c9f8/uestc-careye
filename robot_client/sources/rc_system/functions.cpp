//
// Created by Pulsar on 2021/8/25.
//

#include <rc_system/functions.h>

namespace rccore {
    namespace functions {
        std::string getTimeNow() {
            time_t set_time;
            time(&set_time);
            tm *ptm = localtime(&set_time);
            std::string time_now = std::to_string(ptm->tm_year + 1900)
                                   + "-" + std::to_string(ptm->tm_mon + 1)
                                   + "-" + std::to_string(ptm->tm_mday)
                                   + "_" + std::to_string(ptm->tm_hour)
                                   + ":" + std::to_string(ptm->tm_min)
                                   + ":" + std::to_string(ptm->tm_sec);
            return time_now;
        }
    }
}