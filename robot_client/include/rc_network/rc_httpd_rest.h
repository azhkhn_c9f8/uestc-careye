//
// Created by Pulsar on 2020/5/18.
//

#ifndef UESTC_CAREYE_RC_HTTPD_REST_H
#define UESTC_CAREYE_RC_HTTPD_REST_H

#include <optional>
#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>
#include <algorithm>
#include <ctime>
#include <rc_system/context.h>

namespace rccore {
    namespace network {
        namespace httpd {
            using namespace Pistache;

            class RcHttpdRest {
            public:
                explicit RcHttpdRest(const std::shared_ptr<common::Context> &pcontext);

                void init(size_t thr);

                void start();

                void stop();


            private:
                void setupRoutes();

                void settings(const Rest::Request &request, Http::ResponseWriter response);

                void video(const Rest::Request &request, Http::ResponseWriter response);

                void index(const Rest::Request &request, Http::ResponseWriter response);

                void move(const Rest::Request &request, Http::ResponseWriter response);

                void staticfile(const Rest::Request &request, Http::ResponseWriter response);

                void state_info(const Rest::Request &request, Http::ResponseWriter response);


                std::shared_ptr<Http::Endpoint> httpEndpoint;
                Rest::Router router;
                std::shared_ptr<common::Context> pcontext;
            };
        }
    }
}


#endif // UESTC_CAREYE_RC_HTTPD_REST_H
