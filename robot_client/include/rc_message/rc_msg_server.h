//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RM_MSG_SERVER_H
#define ROBOCAR_RM_MSG_SERVER_H

#include <rc_message/rc_serial_msg.h>
#include <rc_message/rc_image_msg.h>
#include <rc_message/rc_net_work_msg.h>
#include <rc_message/rc_move_msg.h>
#include <rc_message/rc_radar_msg.h>
#include <rc_message/rc_gyro_msg.h>
#include <rc_message/rc_base_msg.hpp>

namespace rccore {
    namespace message {
        class MessageServer {
        public:
            std::shared_ptr<ImageMessage> pimageMessage;//图像消息
            std::shared_ptr<SerialMessage> pserialMessage;//串口消息
            std::shared_ptr<NetworkMessage> pnetworkMessage;//网络消息
            std::shared_ptr<MoveMessage> pmoveMessage;//移动消息
            std::shared_ptr<RadarMessage> pradarMessage;//雷达消息
            std::shared_ptr<GyroMessage> pgyroMessage;//雷达消息
            explicit MessageServer(
                    size_t image_message_queue_size,
                    size_t serial_message_queue_size,
                    size_t gyro_message_queue_size,
                    size_t radar_message_queue_size,
                    size_t network_message_queue_size,
                    size_t move_message_queue_size
            );

            ~MessageServer();
        };
    }
}


#endif //ROBOCAR_RM_MSG_SERVER_H
