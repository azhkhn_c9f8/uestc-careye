#ifndef ROBOCAR_RC_C_GPIO_H
#define SETUP_OK           0
#define SETUP_DEVMEM_FAIL  1
#define SETUP_MALLOC_FAIL  2
#define SETUP_MMAP_FAIL    3
#define SETUP_CPUINFO_FAIL 4
#define SETUP_NOT_RPI_FAIL 5
#define SETUP_EXPORT_FAIL  6

#define RC_GPIO_INPUT  1
#define RC_GPIO_OUTPUT 0
#define ALT0   4

#define RC_GPIO_HIGH 1
#define RC_GPIO_LOW  0

#define PUD_OFF  0

namespace RC {
    namespace GPIO {

        void pinMode(int gpio, int direction);

        void digitalWrite(int gpio, int value);

        void pinFree(int gpio);
    }
}

#endif// ROBOCAR_RC_C_GPIO_H
