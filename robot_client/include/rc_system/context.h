//
// Created by Pulsar on 2021/7/13.
//

#ifndef ROBOCAR_CONTEXT_H
#define ROBOCAR_CONTEXT_H

#include <memory>
#include <rc_system/config.h>
#include <rc_message/rc_msg_server.h>

namespace rccore {
    namespace common {
        class Context {
        public:
            explicit Context(std::shared_ptr<rccore::common::Config> pconfig);

            ~Context();

            void Init();

        public:
            std::shared_ptr<rccore::common::Config> pconfig;
            std::shared_ptr<rccore::message::MessageServer> pmessage_server;
        };
    }
}


#endif //ROBOCAR_CONTEXT_H
