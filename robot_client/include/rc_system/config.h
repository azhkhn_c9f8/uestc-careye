//
// Created by pulsarv on 21-1-29.
//

#ifndef ROBOCAR_CONFIG_H
#define ROBOCAR_CONFIG_H

#include <rc_system/data_struct.h>
#include <string>
#include <memory>

namespace rccore {
    namespace common {
        class Config {
        public:
            explicit Config();

            explicit Config(std::string config_file_path);

            bool generate_config();

            bool Load();

            ~Config();

        public:
            //TODO: 系统信息配置
            std::shared_ptr<data_struct::ConfigInfo> pconfigInfo;
            std::string m_config_file_path;
        };
    } // namespace config
} // namespace follow_down

#endif // ROBOCAR_CONFIG_H
