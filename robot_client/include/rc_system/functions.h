//
// Created by Pulsar on 2021/8/25.
//

#ifndef ROBOCAR_FUNCTIONS_H
#define ROBOCAR_FUNCTIONS_H

#include <future>

namespace rccore {
    namespace functions {
        std::string getTimeNow();
    }
}


#endif //ROBOCAR_FUNCTIONS_H
