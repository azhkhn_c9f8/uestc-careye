
include_directories(${PROJECT_SOURCE_DIR}/src/include)
include_directories(${PROJECT_SOURCE_DIR}/../robot_client/src/include)
message(STATUS "Load Main RobotCenter Application")

set(MAIN_APP_SOUECRS_DIR ${PROJECT_SOURCE_DIR}/src/sources)
set(MAIN_APP_HEADERS_DIR ${PROJECT_SOURCE_DIR}/src/include)
set(MAIN_APP_RESOURCES_DIR ${PROJECT_SOURCE_DIR}/src/resources)
set(MAIN_APP_QML_DIR ${MAIN_APP_RESOURCES_DIR}/qml)

file(GLOB MAIN_APP_SOURCE_FILES
        ${MAIN_APP_SOUECRS_DIR}/*.cpp
        ${MAIN_APP_SOUECRS_DIR}/*.c
        )
file(GLOB MAIN_APP_HEADER_FILES
        ${MAIN_APP_HEADERS_DIR}/*.h
        ${MAIN_APP_HEADERS_DIR}/*.hpp
        ${MAIN_APP_HEADERS_DIR}/*/*.h
        ${MAIN_APP_HEADERS_DIR}/*/*.hpp
        )
qt5_wrap_cpp(MOC_MAIN_APP_HEADER_FILES ${MAIN_APP_HEADER_FILES})
set(MAIN_APP_SOURCE
        ${MAIN_APP_HEADER_FILES}
        ${MAIN_APP_SOURCE_FILES}
        ${MOC_MAIN_APP_HEADER_FILES}
        )

foreach(FILE ${MAIN_APP_SOURCE})
    message(STATUS ${FILE})
endforeach()

file(GLOB MAIN_APP_QML ${MAIN_APP_QML_DIR}/*.qml)
foreach(FILE ${MAIN_APP_QML})
    message(STATUS ${FILE})
endforeach()

qt5_wrap_ui(MIAN_APP_UI_FILES ${MAIN_APP_RESOURCES_DIR}/ui/mainwindow.ui)
qt5_add_resources(MIAN_APP_QRC_FILES ${MAIN_APP_RESOURCES_DIR}/robot_center.qrc)

add_executable(RobotCenter

        ${MAIN_APP_SOURCE}
        ${MIAN_APP_UI_FILES}
        ${MIAN_APP_QRC_FILES}
        ${MAIN_APP_QML}
        )
target_link_libraries(RobotCenter
        rc
        Qt5::Widgets
        Qt5::Core Qt5::Gui Qt5::OpenGL
        Qt5::Network  ${Boost_LIBRARIES}
        center_server
        )
qt5_use_modules(RobotCenter Core Widgets Gui OpenGL Network)